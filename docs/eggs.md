# Eggs

I always love finding the many easter eggs scattered throughout the game. However, I was slacking this year, I only had the time to find one!

## Jason

Jason! One of the first eggs I look for every year! He is always finding good hiding spots. Could be a fish, bird, bucket of nails, or a toilet bowl lever...

Head to Steampunk Island: Coggoggle Marina and you will find him chilling on the beach.

![Jason](images/jason.png)

