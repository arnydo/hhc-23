# Hints

Hints are a huge help while working your way through the Geese Islands. As you complete tasks some of the elves will give you just the nudge you need. Here are all the hints in no particular order.

## Become the Fish

From: Poinsettia McMittens

Objective: 'BONUS! Fishing Guide'

!!! hint
    Perhaps there are some clues about the local aquatic life located in the HTML source code.

## SSH Certificates Talk

From: Alabaster Snowball

Objective: 'Certificate SSHenanigans'

!!! hint
    Check out Thomas Bouve's [talk and demo](https://youtu.be/4S0Rniyidt4) to learn all about how you can upgrade your SSH server configuration to leverage SSH certificates.

## DMARC, DKIM, and SPF, oh my!

From: Fitzy Shortstack

Objective: 'Phish Detection'

!!! hint
    Discover the essentials of email security with DMARC, DKIM, and SPF at [Cloudflare's Guide](https://www.cloudflare.com/learning/email-security/dmarc-dkim-spf/).

## Comms Journal

From: Chimney Scissorsticks

Objective: 'The Captain's Comms'

!!! hint
    I've seen the Captain with [his Journal](https://elfhunt.org/static/images/captainsJournal.png) visiting Pixel Island! 

## Comms JWT Intro

From: Chimney Scissorsticks

Objective: 'The Captain's Comms'

!!! hint
    A great introduction to JSON Web Tokens is available from [Auth0](https://jwt.io/introduction).

## Gameboy 2

From: Tinsel Upatree

Objective: 'Game Cartridges: Vol 2'

!!! hint
    1) This feels the same, but different!  2) If it feels like you are going crazy, you probably are! Or maybe, just maybe, you've not yet figured out where the hidden ROM is hiding. 3)  I think I may need to get a DIFFerent perspective.  4) I wonder if someone can give me a few pointers to swap.

## Lock Talk

From: Garland Candlesticks

Objective: 'Luggage Lock Decode'

!!! hint
    Check out Chris Elgee's [talk](https://youtu.be/ycM1hBSEyog) regarding his and his wife's luggage.  Sounds weird but interesting!

## Outbound Connections

From: Tangle Coalbox

Objective: 'KQL Kraken Hunt'

!!! hint
    Do you need to find something that happened via a process?  Pay attention to the _ProcessEvents_ table!

## Azure VM Access Token

From: Sparkle Redberry

Objective: 'Certificate SSHenanigans'

!!! hint
    Azure CLI tools aren't always available, but if you're on an Azure VM you can always use the [Azure REST API](https://learn.microsoft.com/en-us/entra/identity/managed-identities-azure-resources/how-to-use-vm-token) instead.

## Synthesis is the True Ending

From: Santa

Objective: ''

!!! hint
    The AI revolution has begun. Some of the most prominent and *useful* tools born from the advent of powerful AI include [ChatGPT](https://chat.openai.com/auth/login), [PlayHT](https://play.ht/), [Midjourney](https://www.midjourney.com/home?callbackUrl=%2Fexplore), [Dall-E 3](https://openai.com/dall-e-3), [Bing AI](https://www.bing.com/search?form=MY0291&OCID=MY0291&q=Bing+AI&showconv=1), and [Bard](https://bard.google.com/?utm_source=sem&utm_medium=paid-media&utm_campaign=q4enUS_sem7&gclid=Cj0KCQiAgqGrBhDtARIsAM5s0_mJZWSITeLgVbaMSfn_6hubm-ydPDgylkxdJHHbNT1Wjy3dpUd3aVMaAgdsEALw_wcB), and [Grok](https://grok.x.ai/).

## I Am Become Data

From: Poinsettia McMittens

Objective: 'BONUS! Fishing Guide'

!!! hint
    One approach to automating web tasks entails the browser's developer console. Browsers' console allow us to manipulate objects, inspect code, and even interact with [websockets](https://javascript.info/websocket).

## KQL Tutorial

From: Tangle Coalbox

Objective: 'KQL Kraken Hunt'

!!! hint
    Once you get into the [Kusto trainer](https://detective.kusto.io/sans2023), click the blue _Train me for the case_ button to get familiar with KQL.

## JWT Secrets Revealed

From: Piney Sappington

Objective: 'Elf Hunt'

!!! hint
    Unlock the mysteries of JWTs with insights from [PortSwigger's JWT Guide](https://portswigger.net/web-security/jwt).

## Linux Command Injection

From: Rose Mold

Objective: 'Linux PrivESC'

!!! hint
    Use the privileged binary to overwriting a file to escalate privileges could be a solution, but there's an easier method if you pass it a crafty argument.

## Comms Private Key

From: Chimney Scissorsticks

Objective: 'The Captain's Comms'

!!! hint
    Find a private key, update an existing JWT!

## Snowball Super Hero

From: Morcel Nougat

Objective: 'Snowball Hero'

!!! hint
    Its easiest to grab a friend play with and beat Santa but tinkering with client-side variables can grant you all kinds of snowball fight super powers. You could even take on Santa and the elves solo!

## Consoling iFrames

From: Morcel Nougat

Objective: 'Snowball Hero'

!!! hint
    Have an iframe in your document? Be sure to [select the right context](https://gist.github.com/chrisjd20/93771da596ca5e49043f148a845c469f) before meddling with JavaScript.

## MFA: Something You Are

From: Jewel Loggins

Objective: 'Space Island Access Speaker'

!!! hint
    It seems the Access Speaker is programmed to only accept Wombley's voice. Maybe you could get a sample of his voice and use an AI tool to simulate Wombley speaking the passphrase.

## Buried Treasures

From: Dusty Giftwrap

Objective: ''

!!! hint
    There are 3 buried treasures in total, each in its own uncharted area around Geese Islands. Use the gameboy cartridge detector and listen for the sound it makes when treasure is nearby, which gets louder the closer you are. Also look for some kind of distinguishing mark or feature, which could mark the treasure's location.

## Fishing Machine

From: Poinsettia McMittens

Objective: 'BONUS! Fishing Guide'

!!! hint
    There are a variety of strategies for automating repetative website tasks. Tools such as [AutoKey](https://github.com/autokey/autokey) and [AutoIt](https://www.autoitscript.com/site/) allow you to programmatically examine elements on the screen and emulate user inputs.

## File Creation

From: Tangle Coalbox

Objective: 'KQL Kraken Hunt'

!!! hint
    Looking for a file that was created on a victim system?  Don't forget the _FileCreationEvents_ table.

## Hubris is a Virtue

From: Wombley Cube

Objective: 'Camera Access'

!!! hint
    In his hubris, Wombley revealed that he thinks you won't be able to access the satellite's "Supervisor Directory". There must be a good reason he mentioned that specifically, and a way to access it. He also said there's someone else masterminding the whole plot. There must be a way to discover who that is using the nanosat.

## Gameboy 1

From: Dusty Giftwrap

Objective: 'Game Cartridges: Vol 1'

!!! hint
    1) Giving things a little push never hurts.  2) Out of sight but not out of ear-shot  3) You think you fixed the QR code? Did you scan it and see where it leads?

## Useful Tools

From: Ribb Bonbowford

Objective: 'Active Directory'

!!! hint
    It looks like Alabaster's SSH account has a couple of tools installed which might prove useful.

## Comms Web Interception Proxies

From: Chimney Scissorsticks

Objective: 'The Captain's Comms'

!!! hint
    Web Interception proxies like [Burp](https://portswigger.net/burp) and [Zap](https://www.zaproxy.org) make web sites fun!

## Linux Privilege Escalation Techniques

From: Rose Mold

Objective: 'Linux PrivESC'

!!! hint
    There's [various ways](https://payatu.com/blog/a-guide-to-linux-privilege-escalation/) to escalate privileges on a Linux system. 

## Azure Function App Source Code

From: Alabaster Snowball

Objective: 'Certificate SSHenanigans'

!!! hint
    The [get-source-control](https://learn.microsoft.com/en-us/rest/api/appservice/web-apps/get-source-control) Azure REST API endpoint provides details about where an Azure Web App or Function App is deployed from.

## Always Lock Your Computer

From: Wombley Cube

Objective: 'Satellite Ground Station Control Panel'

!!! hint
    Wombley thinks he may have left the admin tools open. I should check for those if I get stuck.

## Comms Abbreviations

From: Chimney Scissorsticks

Objective: 'The Captain's Comms'

!!! hint
    I hear the Captain likes to abbreviate words in his filenames; shortening some words to just 1,2,3, or 4 letters.

## Uncharted

From: Rose Mold

Objective: ''

!!! hint
    Not all the areas around Geese Islands have been mapped, and may contain wonderous treasures. Go exploring, hunt for treasure, and find the pirate's booty!

## Approximate Proximity

From: Dusty Giftwrap

Objective: 'Game Cartridges: Vol 1'

!!! hint
    Listen for the gameboy cartridge detector's proximity sound that activates when near buried treasure. It may be worth checking around the strange toys in the Tarnished Trove.

## Gameboy 2

From: Tinsel Upatree

Objective: 'Game Cartridges: Vol 2'

!!! hint
    Try poking around Pixel Island.  There really aren't many places you can go here, so try stepping everywhere and see what you get!

## Bird's Eye View

From: Angel Candysalt

Objective: 'Game Cartridges: Vol 3'

!!! hint
    The location of the treasure in Rusty Quay is marked by a shiny spot on the ground. To help with navigating the maze, try zooming out and changing the camera angle.

## Gameboy 3

From: Angel Candysalt

Objective: 'Game Cartridges: Vol 3'

!!! hint
    1) This one is a bit long, it never hurts to save your progress!  2) 8bit systems have much smaller registers than you├ó┬Ç┬Öre used to.  3) Isn├ó┬Ç┬Öt this great?!? The coins are OVERFLOWing in their abundance.
