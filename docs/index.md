# Introduction

IT IS TIME! THE TIME! THE BEST TIME OF THE YEAR!

Yes, I get a little excited for Holiday Hack Challenge. We are at the point where I can now tell my wife, hey...HHC is starting next week...and she knows to expect me to be laser focused on helping Santa, banging my head (both literally due to the growing pains and due to the AWESOME soundtrack put out ever year), and working withing the community to help others do the same.

HHC has a special place in my heart as I know how much time, effort, and care goes into the event and the team behind it. I can say HHC has had a major impact on my carrer since I first played years ago and I am thankful for it. 

![glory](images/glory.png)

It isn't near as thourough as I had intended but it is my hope that this writup captures just a glimpse of the experience that is gained by working through these tasks and helps remind me of the lessons learned and hopefully teaches somebody else something new.

If you have any questions or feedback please feel free to reach out!

Sincerly,

Arnydo - Kyle Parrish

---

I ALMOST FORGOT! One of the reasons I didn't have the time to focus on my writup is because I got sidetracked building a web version of the Pescadex for easy viewing of your favorite catches this year!!

Check it out here: [https://arnydo.gitlab.io/2023-hhc-pescadex](https://arnydo.gitlab.io/2023-hhc-pescadex)

![Pescadex](images/pescadex.png)